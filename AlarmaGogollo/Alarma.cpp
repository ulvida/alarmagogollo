/* 
 *  Gestión del tiempo de advertencia y disparo de alarma
 */
#include "Alarma.h"

Alarma::Alarma(int pinAdvertencia, int pinAlarma) {
  PinAdvertencia = pinAdvertencia;
  PinAlarma = pinAlarma;
  pinMode(PinAdvertencia, OUTPUT);
  digitalWrite(PinAdvertencia, LOW);
  pinMode(PinAlarma, OUTPUT);
  digitalWrite(PinAlarma, LOW);
  Retraso = 32767;
  Advertencia = false;
  AdvertenciaEstado = LOW;
  InstanteDisparo = 0;
  InstanteCambioEstado = millis();
  InstanteUltimoMensaje  = millis();
  Sirena = false;
}

void Alarma::Disparar(int retraso) {
  unsigned long instanteActual;
  instanteActual = millis();
  if ( !Advertencia ) {
    Advertencia = true;
    Retraso = retraso; 
  } else if ( instanteActual + retraso < InstanteDisparo + Retraso ) {
    Retraso = retraso; 
  }
  InstanteDisparo = instanteActual;
}

void Alarma::Desarmar() {
  Advertencia = false;
  digitalWrite(PinAdvertencia, LOW);
  Sirena = false;
  digitalWrite(PinAlarma, LOW);
}

void Alarma::TicTac() {
  unsigned long instanteActual;
  if ( !Sirena && Advertencia ) {
    instanteActual = millis();
    if ( (AdvertenciaEstado == HIGH) &&  (instanteActual - InstanteCambioEstado > BIP_ON) ) {
      AdvertenciaEstado = LOW;
      InstanteCambioEstado = instanteActual;
      digitalWrite(PinAdvertencia, LOW);
    } else if ( (AdvertenciaEstado == LOW) &&  (instanteActual - InstanteCambioEstado > BIP_OFF) ) {
      AdvertenciaEstado = HIGH;
      InstanteCambioEstado = instanteActual;
      digitalWrite(PinAdvertencia, HIGH);
    }
    if ( instanteActual >= InstanteUltimoMensaje + 1000) {
      Mensaje = true;
      InstanteUltimoMensaje = instanteActual;
    }
    if (instanteActual > InstanteDisparo + Retraso) {
      #ifdef _DEBUG_
        Serial.println("Disparo sirena!");
      #endif
      Sirena = true;
      digitalWrite(PinAlarma, HIGH);
      digitalWrite(PinAdvertencia, LOW);      
    }
  }
}

