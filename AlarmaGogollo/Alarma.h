  #ifndef _Alarma_h
#define _Alarma_h
/* 
 *  Gestión del tiempo de advertencia y disparo de alarma
 */

#include <Arduino.h> 
#include "AlarmaGogolloLib.h"

#define BIP_ON 100
#define BIP_OFF 400

class Alarma {
  public:
  int PinAdvertencia;
  int PinAlarma;
  int Retraso;
  bool Advertencia;
  unsigned long InstanteDisparo;
  bool Mensaje;
  unsigned long InstanteUltimoMensaje;
  bool AdvertenciaEstado;
  unsigned long InstanteCambioEstado;
  bool Sirena;

  public:
  Alarma(int pinAdvertencia, int pinAlarma);
  void Disparar(int retraso);
  void Desarmar();
  void TicTac();
  
};
 
#endif
