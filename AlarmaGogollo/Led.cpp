/*
 *  Gestión de leds parpadeantes, con diferentes períodos
 */

#include "Led.h"
Led::Led(int pin) {
  ledPin = pin;
  pinMode(ledPin, OUTPUT);     
  
  TiempoOn = 800;
  TiempoOff = 400;

  ledState = LOW; 
  previousMillis = millis();
}

void Led::Parpadear() {
  // check to see if it's time to change the state of the LED
  unsigned long currentMillis = millis();
   
  if ( (ledState == HIGH) && (currentMillis - previousMillis >= TiempoOn) )
  {
    ledState = LOW;  // Turn it off
    previousMillis = currentMillis;  // Remember the time
    digitalWrite(ledPin, ledState);  // Update the actual LED
  }
  else if ((ledState == LOW) && (currentMillis - previousMillis >= TiempoOff))
  {
    ledState = HIGH;  // turn it on
    previousMillis = currentMillis;   // Remember the time
    digitalWrite(ledPin, ledState);   // Update the actual LED
  }
}

void Led::CambiarPeriodos( long on, long off) {
  TiempoOn = on;
  TiempoOff = off;
} 
