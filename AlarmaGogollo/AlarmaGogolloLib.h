#ifndef _AlarmaGogolloLib_h
#define _AlarmaGogolloLib_h
/*
 *  Gestión de alarma con sensor de aberturas y barreras electrónicas con arduino
 *  Distribuido bajo licencia GPL
 *  Código: (c) Ulvida daniel@uruguayos.fr
 *  Realización material: (c) Pablo Fagundez & Ulvida
 */

 /************************************
  * Algunas constantes compartidas con las librerías
  */

// una constante _DEBUG_ que activa o desactiva los mensajes en serial
// #define _DEBUG_
// #define _INFO_DEBUG_ "Valor del pin 8:"
// #define _PIN_DEBUG_ 8
#endif
