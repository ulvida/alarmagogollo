#ifndef _Led_h
#define _Led_h
/*
 *  Gestión de leds parpadeantes, con diferentes períodos
 */

#include <Arduino.h> 
#include "AlarmaGogolloLib.h"

class Led {
  
  // Variables Miembros de la Clase
  // Estas se inicializan al empezar
  int ledPin;      // the number of the LED pin
  long TiempoOn;     // milliseconds of on-time
  long TiempoOff;    // milliseconds of off-time
 
  // Estas mantienen el estado
  int ledState;                 // ledState used to set the LED
  unsigned long previousMillis;   // will store last time LED was updated
 
  public:

  // Constructor - creata una led que parpadea
  // e inicializa las variables miembros y el estado 
 // Led(int pin, int on, int off) {
 // Led(int, int, int);
 // Led(int pin) {
  Led(int);
    
 
  void Parpadear();

  void CambiarPeriodos( long, long);

};
#endif
