/*
 *  Gestión de alarma con sensor de aberturas y barreras electrónicas con arduino
 *  Distribuido bajo licencia GPL
 *  Código: (c) Ulvida daniel@uruguayos.fr
 *  Realización material: (c) Pablo Fagundez & Ulvida
 */


#include "AlarmaGogollo.h"


/****************************
 * funciones para manejar la pantalla
 */
 
void pantallaEscribir (char pantallaMsg[L_LCD][C_LCD + 1]) {
  int i;
  pantalla.init();
  pantalla.backlight();
  for (i=0; i < L_LCD ; i++) {
    pantalla.setCursor(0,i);
    pantalla.print(pantallaMsg[i]);
    #ifdef _DEBUG_
      Serial.println(pantallaMsg[i]);
    #endif
  }
  #ifdef _DEBUG_
    Serial.println("       ******");
  #endif 
}

void pantallaApagar() {
  pantalla.noDisplay();
  pantalla.noBacklight();  
}

/****************************
 * funciones de armado de alarma
 */

void alarmaArmar() {
  armado = true;
  led.CambiarPeriodos(ledPeriodoArmadoOn,ledPeriodoArmadoOff); 
  botonArmado.attachDoubleClick(NULL);
  botonArmado.attachClick(alarmaDisparar);
  sprintf(pantallaAlarmaArmar[1], plantillaTiempoSalida, tiempoSalida );
  pantallaEscribir(pantallaAlarmaArmar);
  delay(tiempoSalida * 1000);
  pantallaEscribir(pantallaAlarmaActivar);
  delay(1000);
  pantallaApagar();
}

void alarmaDesarmar() {
  int i, j;
  #ifdef _DEBUG_
    Serial.println("Desarmar alarma");
  #endif
  led.CambiarPeriodos(ledPeriodoDesarmadoOn,ledPeriodoDesarmadoOff);
  alarma.Desarmar();
  armado = false;
  alarmaReportada = false;
  for (i=0; i<NB_SENSORES; i++) {
    sensor[i].Disparado = false;
  }
  pantallaEscribir(pantallaAlarmaLista);
  #ifdef _DEBUG_
    Serial.println("Alarma lista...");
  #endif
  botonArmado.attachDoubleClick(alarmaArmar);
  botonArmado.attachClick(NULL);
  while( ! armado ) {
    // esperamos un doble click para armar la alarma
    //Serial.println("Seguimos esperando...");
    botonArmado.tick();
    led.Parpadear();
  }  
 }

void alarmaDisparar() {
  digitalWrite(PIN_ALARMA, HIGH);
  #ifdef _DEBUG_
    Serial.println("Alarma!! Alarma!! Alarma!! Alarma!!");
  #endif
  pantallaEscribir(pantallaAlarmaSonar);  
}

void setup() {
  int i, j;
  #ifdef _DEBUG_
    Serial.begin(9600);
    Serial.println("arrancando");
  #endif
  pantalla.begin(C_LCD, L_LCD);  
  pantallaEscribir(pantallaBienvenida);
  
  // Vigilamos el botón de armado
//   pinMode(BOTON_ARMADO,INPUT_PULLUP); //???verificar en la lib --> ahora está en el llamado del boton
  pinMode(PIN_ALARMA, OUTPUT);

  delay(2000);
    sprintf(pantallaSensores[0], plantillaNbSensores, NB_SENSORES);
  for (i=0; i<NB_SENSORES; i++) {
    j = i%3;
    sprintf(pantallaSensores[j+1], sensorNombres[i]);
    if ( j == 3) {
      pantallaEscribir(pantallaSensores);
      delay(1000);
    }
  }
  if (j != 3 ) {
    pantallaEscribir(pantallaSensores);
    delay(1000);
  }
  alarmaDesarmar();

}

void loop() {
  int i,j;
  led.Parpadear();
  botonArmado.tick();
  alarma.TicTac();
/*  if ( alarma ) {
    alarmaDisparar();
  } */ 
  #ifdef _DEBUG_  
    Serial.print(_INFO_DEBUG_);
    Serial.println(digitalRead(_PIN_DEBUG_));
  #endif
  for ( i=0; i<NB_SENSORES; i++) {
    sensor[i].Escuchar();
    if ( !alarma.Sirena && !sensor[i].Disparado && sensor[i].Disparo ) {
      sensor[i].Disparado = true;
      alarma.Disparar(sensor[i].TiempoAdvertencia);
      led.CambiarPeriodos(ledPeriodoAdvertenciaOn,ledPeriodoAdvertenciaOff); 
      sprintf(pantallaAlarmaDisparar[0], plantillaAlarmaDispararSensor, sensor[i].Nombre);
      // sprintf(pantallaAlarmaDisparar[1], plantillaAlarmaDispararTiempo, sensor[i].TiempoAdvertencia);
      #ifdef _DEBUG_
        Serial.print("Sensor: ");
        Serial.println(sensor[i].Nombre);
        Serial.print("Tiempo advertencia: ");
        Serial.println(sensor[i].TiempoAdvertencia);
      #endif
    }
  }
  if ( alarma.Mensaje ) {
    sprintf(pantallaAlarmaDisparar[1], plantillaAlarmaDispararTiempo, (alarma.InstanteDisparo + alarma.Retraso - millis() )  / 1000 );
    pantallaEscribir(pantallaAlarmaDisparar);
    alarma.Mensaje = false;
  }
  if ( !alarmaReportada && alarma.Sirena ) {
    alarmaReportada = true;
    pantallaEscribir(pantallaAlarmaSonar);
  }
  if ( botonDesarme.Escuchar() ) {
    #ifdef _DEBUG_
      Serial.println("reinicio alarma");
    #endif
    alarmaDesarmar();
  }

}
