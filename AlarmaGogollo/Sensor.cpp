/*
 * Gestión de un sensor de tipo interruptor pasivo
 * En lo concretos usamos: 
 * - sensores electromecánicos magnéticos para la puerta y la ventana
 * - sensores electrónicos infrarojos con alimentación separada para barreras elecrtrónicas. 
 */

#include "Sensor.h"

Sensor::Sensor(int sensorPin, char* sensorNombre, int retrasoAdvertencia, bool invertido) {

  Pin = sensorPin;
  TiempoAdvertencia = retrasoAdvertencia;
  InstanteAdvertencia = 0;
  pinMode(Pin, INPUT_PULLUP);
  Nombre = sensorNombre;
  Disparo = false;
  Disparado = false;
  Invertido = invertido;
}

bool Sensor::Escuchar() {
  if ( (Invertido && digitalRead(Pin) == HIGH) || (!Invertido && digitalRead(Pin) == LOW) ) {
    Disparo = true;
  } else {
    Disparo = false;
  }
  if ( !Disparado ) {
    InstanteAdvertencia = millis();
  }
  return Disparo;
}
