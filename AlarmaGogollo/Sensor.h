#ifndef _Sensor_h
#define _Sensor_h
/*
 * Gestión de un sensor de tipo interruptor pasivo
 * En lo concretos usamos: 
 * - sensores electromecánicos magnéticos para la puerta y la ventana
 * - sensores electrónicos infrarojos con alimentación separada para barreras elecrtrónicas. 
 */
#include <Arduino.h> 
#include "AlarmaGogolloLib.h"


class Sensor {

  // Variables Miembros de la Clase
  // Estas se inicializan al empezar
public:
  int Pin;
  char* Nombre;
  int TiempoAdvertencia;

  // Variables de estados
  // Esta se activa al disparar
  bool Invertido;
  bool Disparo, Disparado;
  // y su instante
  unsigned long InstanteAdvertencia;

  public:
  // Constructor
  // Sensor(int pin, char* sensor, int retrasoAdvertencia) {
  Sensor(int , char* , int, bool);
  
  bool Escuchar();
    
};
#endif
