/*
 *  Gestión de alarma con sensor de aberturas y barreras electrónicas con arduino
 *  Distribuido bajo licencia GPL
 *  Código: (c) Ulvida daniel@uruguayos.fr
 *  Realización material: (c) Pablo Fagundez & Ulvida
 */
 
 /********************************************************
  * Librerías y clases propias
  */

#include "AlarmaGogolloLib.h"

// Una clase propia que gestiona una led parpadeante. 
#include "Led.h"

// Hay varias librerías para botones (ej Button: https://github.com/tigoe/Button)
// l final elejimos OneButton: https://github.com/mathertel/OneButton
// Previamente se debe instalar en el IDE esta librería (descrgar el .zip y cargarlo en el IDE, por ejemplo)
#include <OneButton.h>

// Una clase propia que gestiona los sensores, todos de tipo interruptor pasivo (lo que implica configurarlos en INPUT_PULLUP)
#include "Sensor.h"

// Una librería para gestionar el la alarma, con un retraso desde el disparo. 
#include "Alarma.h"

/* Wire (necesario para comunicar en I2C con la pantalla led)
 * https://www.arduino.cc/en/Reference/Wire acá están particular descrita la conectividad del I2C: 
 * A4 <-> SDA
 * A5 <-> SCL
 * GND y VCC a la alim.
 */
#include <Wire.h>  // Comes with Arduino IDE

// Librería para la pantalla LCD con interfaz I2C
/* Al final usamos esta: https://github.com/marcoschwartz/LiquidCrystal_I2C, partiendo de los ejemplos. 
 * Hay que correr I2C scanner: https://playground.arduino.cc/Main/I2cScanner 
 * para encontrar la dirección de la interfaz I2C: 0x3F 
 * Hay otras librerías. Una que me pareció en un momento que funcionó... _
 * https://bitbucket.org/fmalpartida/new-liquidcrystal/downloads
 * La librería https://bitbucket.org/fmalpartida/new-liquidcrystal/wiki/Home
 */
#include <LiquidCrystal_I2C.h>

/********************************************************
 * Parámetros del sistema de alarma:
 */

// el tiempo que se tiene para salir cuando se activa la alarma (en segundos)
const int tiempoSalida = 10;

// const bool debug = true;

// boolenaos de estado
// bool armado = false, alarma = false, advertencia = false;
// bool armado = false, advertencia = false;
bool armado = false, alarmaReportada = false;

/******************************* 
 *  para Pantalla.h 
 *******************************/

#define L_LCD 4
#define C_LCD 20
// cmbiamos de pantalla, 0x3F era la otra
#define ADDR_LCD 0x27  


// PIN de activación de la advertenica
// #define BOTON_ARMADO 10
#define BOTON_ARMADO A1


// definición de PINs de los sensores
/* digamos que la casa está al oeste, y que, desde 2 esquinas rodeamos el campo con 3 barreras laser 
 * lo que sigue son los PINS utilizados. En producción pensamos en: 

#define NB_SENSORES 7

#define SENSOR_PUERTA 0
#define SENSOR_VENTANA 1
#define BARRERA_HACIA_CASA 2
#define BARRERA_DIAGONAL 3
#define BARRERA_FRENTE 4
#define BARRERA_NORTE 5
#define BARRERA_ENTRADA_CLUB 6
 *
 * Por ahora porbamos con dos sensores: una barrera y un sensor magnético de abertura. 
 */

// Cantidad total de sensores
#define NB_SENSORES 7

// Números de Pins de los sensores
#define SENSOR_PUERTA 2
#define SENSOR_VENTANA 3
// los sensores barrera electrónica, en sentido anti-horario, empezando del lado este (casa)
#define BARRERA_ESTE 4     // lado de la casa
#define BARRERA_NORESTE 5  // diagonal
#define BARRERA_NORTE 6
// el oeste se desactiva
#define BARRERA_OESTE 7
#define BARRERA_SUR 8      // entrada al club



// tiempo de retraso en disparar la alarma para cada sensor 
// (en millisegundos, no funciona con 0, min 1000?). 
#define RETRASO_PUERTA 10000
#define RETRASO_VENTANA 1000
#define RETRASO_ESTE 20000
#define RETRASO_NORESTE 1000
#define RETRASO_NORTE 1000
#define RETRASO_OESTE 1000
#define RETRASO_SUR 20000

// a continuación quizás se pueda simplificar las indirecciones, dejando sólo un nivel de constantes

// Los cuadros que siguen deben ser inicializados con NB_SENSORES valores
// Nombre de los sensores (para la pantalla, luegoconst )
// const char* sensorNombre[NB_SENSORES] = { "Sensor puerta" };  
//char sensorNombres[NB_SENSORES][C_LCD+1] = { "Puerta", "Infrarojo" };  
//char sensorNombres[NB_SENSORES][C_LCD+1] = { "Puerta", "Ventana", "IR sur" };  
//char sensorNombres[NB_SENSORES][C_LCD+1] = { "Puerta", "Ventana" };  
char sensorNombres[NB_SENSORES][C_LCD+1] = { "Puerta", "Ventana", "IR este", "IR noreste", "IR norte", "IR oeste", "IR sur" };  

// PIN de de reseteo del arduino
#define PIN_DESARMAR 10

// PIN de activación de la advertenica
#define PIN_ADVERTENCIA 11

// PIN de activación de la Alarma
#define PIN_ALARMA 12

// PIN de la Led
#define PIN_LED 13

// los tiempos de parpadeo de la led en cada estado
static const int ledPeriodoDesarmadoOn = 800;
static const int ledPeriodoDesarmadoOff = 400;
static const int ledPeriodoArmadoOn = 3000;
static const int ledPeriodoArmadoOff = 1000;
static const int ledPeriodoAdvertenciaOn = 200;
static const int ledPeriodoAdvertenciaOff = 50;
static const int ledPeriodoAlarma = 50;


// las pantallas y plantillas de mensajes en la pantalla, de 20 car max

char pantallaBienvenida[L_LCD][C_LCD + 1] = {"Hola Gogollero!", "Soy Little Brother,", "el hermanito buchon.", "Arrancando..."};

char plantillaNbSensores[] = "%d sensores:";
char pantallaSensores[L_LCD][C_LCD + 1] = {"", "", "", ""};
char pantallaAlarmaLista[L_LCD][C_LCD + 1] = {"Alarma lista.", "Doble click en el", "boton rojo", "para activar."};

char plantillaTiempoSalida[] = "Tienes %d segundos";
char pantallaAlarmaArmar[L_LCD][C_LCD + 1] = {"Alarma armada!", "", "para salir.", ""};
char pantallaAlarmaActivar[L_LCD][C_LCD + 1] = {"Alarma activada!", "Sensores armados!", "", ""};
char plantillaAlarmaDispararSensor[] = "Disparo: %s!";
char plantillaAlarmaDispararTiempo[] = "Tienes %d segudos";
char pantallaAlarmaDisparar[L_LCD][C_LCD + 1] = {"Disparo: %s!", "Tienes %d segundos","para desactivar","la alarma."};
char pantallaAlarmaSonar[L_LCD][C_LCD + 1] = {"Alarma! Alarma!", "Alarma! Alarma!", "Alarma! Alarma!", "Alarma! Alarma!"};


/******************************************************
 * La declaración misma de instancias de clases
 * ***************************************************
 * 
 * Modelizamos en objetos (librerías .h/.cpp propias: 
 * - la led parpadeante a rítmos configurables
 * - los sensores, que son todos actuadores vistos como interruptores pasivos 
 * Usamos librerías de terceros para: 
 * - el manejo de varios eventos de un botón: OneButton
 * - el manejo de la pantalla líquida (Ojo, es re difícil encontrar la librería, la q anda es la que dice qu no está mantenida: 
 *   https://github.com/marcoschwartz/LiquidCrystal_I2C
 *   
 */

// Uno solo led que parpadea a diferentes rítmos
Led led(PIN_LED);

// Un botón de armado y desarmado de la alarma
OneButton botonArmado(BOTON_ARMADO, true, true);

// La pantalla Led
LiquidCrystal_I2C pantalla(ADDR_LCD,L_LCD,C_LCD); 

// NB_SENSORES instancias de sensores, asociados a sus respectivos pins
Sensor sensor[NB_SENSORES] = {
  Sensor(SENSOR_PUERTA, sensorNombres[0], RETRASO_PUERTA, true),
  Sensor(SENSOR_VENTANA, sensorNombres[1], RETRASO_VENTANA, true),
  Sensor(BARRERA_ESTE, sensorNombres[2], RETRASO_ESTE, true),
  Sensor(BARRERA_NORESTE, sensorNombres[3], RETRASO_NORESTE, true),
  Sensor(BARRERA_NORTE, sensorNombres[4], RETRASO_NORTE, true),
   Sensor(BARRERA_OESTE, sensorNombres[5], RETRASO_OESTE, true),
  Sensor(BARRERA_SUR, sensorNombres[6], RETRASO_SUR, true)
};

// El gestor de la Alarma y su retraso entre disparo y sirena
Alarma alarma(PIN_ADVERTENCIA, PIN_ALARMA);

char nombreNotonDesarme[] = "Desarmar"; 

Sensor botonDesarme(PIN_DESARMAR,nombreNotonDesarme,0, true);

