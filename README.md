# Alarma del Gogollo Alegre

(c) Daniel Viñar daniel@uruguayos.fr & Pablo Fagundez lapelaproducciones@gmail.com
GPLv3

## Alarma de un predio de plantación

La alarma comprende 7 sensores: 5 barreras infrarojas que delimitan el predio de plantación, y dos sensores magnéticos en la puerta y la ventana de la pieza del club. 

El funcionamiento es muy simple: se pone en tensión, y se enciende la led. 
Con doble clic en el botón rojo (conectado al PIN A1) se activa la alarma. 
Se tiene entonces 30 segundos (parametrable en el programa) para salir antes que se activen los sensores. 

Los sensores pueden tener un retraso en la activación, durante el cual se activará la advertencia. 
Si antes de transcurrido ese retraso la persona no desactiva la alarma con el botón oculto (eventualmente bajo llave), la alarma se desencadena. 

## El programa

El programa es un sketch que funciona sobre un arduino, y maneja sensores de tipo interruptor: se los conecta a un pin en INPUT_PULLUP, están cerrados (por ende a LOW) cuando el sensor está en alerta, no activado, pasa a HIGH cuando el sensor detecta una intrusión.

El programa es capaz de manejar N sensores diferentes, el retraso de alarma de cada uno pudiendo ser diferente. El número de sensores está limitado por el número de pins del arduino. En el encabezado se define el número de sensores, los pines asociados, el nombre de los sensores y su retraso a la alarma. 

También está en parámetro el tiempo que hay para salir del recinto protegido al activar la alarma. 

## Modificaciones posibles de parámetros

Los parámetros que se pueden modificar están todos en el archivo  [AlarmaGogollo.h](AlarmaGogollo/AlarmaGogollo.h) (pero OjO: no todos los parámetros de 
ese archivo se pueden modificar, y hay que modificarlos coherentemente):

* el [tiempo de salida](AlarmaGogollo/AlarmaGogollo.h#L51) que deja la alarma al armarse, antes de que active los sensores, 
* los [numeros de pins a los cuales están conectados los sensores](AlarmaGogollo/AlarmaGogollo.h#L96), que se definen como constantes de compilación,
* los [tiempos de retraso luego de disparo de cada sensor](AlarmaGogollo/AlarmaGogollo.h#L109), en milisegundos,
* la [cantidad del sensores](AlarmaGogollo/AlarmaGogollo.h#L93), pero OjO que ahí hay que modificar en coherencia la declaración de los sensores y su nombre: 
* * los [nombres de sensores](AlarmaGogollo/AlarmaGogollo.h#L126) se declaran como un array de cadenas de caracteres (char[]). Debe haber exactamente tantos nombre de inicialización del cuadro como cantidad de sensores declarada, 
* * la [declaración de los sensores](AlarmaGogollo/AlarmaGogollo.h#L191) se declaran como un array de instancias de la clase de sensores, que deben ser inicializados llamados al constructor de la clase con los parámetros que correspondan. 
* * no es en sí necesario que se borren declaraciones de pins de sensores o de atrasos que no se usen. 

## Debug

En el archivo [AlarmaGogolloLib.h](AlarmaGogollo/AlarmaGogolloLib.h) se puede descomentar la definición de una constante de compilación _DEBUG_, que provocará el reporte en el monitor serial del arduino de múltiples mensajes de debug en el programa. 

Otrs dos variables permiten el debug directo de alguno de los pins inicializados. (OjO: el valor del pin debe estar considerado en los sensores, para que la lectura sea válida. OjO: hay que definir esas constantes si se activa el debug) 